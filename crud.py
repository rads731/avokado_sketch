from crud import app
from os import getenv

port = int(getenv('PORT', 5000))
if __name__ == "__main__":
    app.run(debug = True, host='0.0.0.0', port=port, threaded=True)